import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import color from "constants/color";

interface ItemRecordEntity {
  img: string;
  title: string;
  subTitle: string;
}
function ItemRecord({ img, title, subTitle }: ItemRecordEntity) {
  return (
    <Box width={288} height={288} p={3} bgcolor={color.PRIMARY}>
      <Box
        width='100%'
        height='100%'
        display='flex'
        justifyContent='center'
        alignItems='center'
        flexDirection='column'
        position='relative'
      >
        <Box
          component='img'
          src={img}
          width='100%'
          height='100%'
          alt='imgrecord'
          sx={{
            objectFit: "cover",
            filter: "grayscale(100%)"
          }}
        />
        <Box
          display='flex'
          justifyContent='center'
          alignItems='center'
          flexDirection='column'
          position='absolute'
          width='100%'
          top='50%'
          left='50%'
          sx={{
            transform: "translate(-50%, -50%)"
          }}
        >
          <Typography
            fontSize='2.5rem'
            fontWeight={400}
            sx={{
              textTransform: "uppercase"
            }}
            color='primary300.main'
          >
            {title}
          </Typography>
          <Box width={160} height={24} bgcolor='primary400.main' mt={1.375}>
            <Typography fontSize='1.4rem' fontWeight={300} color='light.main' align='center'>
              {subTitle}
            </Typography>
          </Box>
        </Box>
      </Box>
    </Box>
  );
}

export default ItemRecord;
