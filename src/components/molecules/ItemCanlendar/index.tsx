import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import color from "constants/color";

interface ItemCalendarEntity {
  title: string;
  desc: string;
}
function ItemCanlendar({ title, desc }: ItemCalendarEntity) {
  return (
    <Box width='100%' display='flex' p={2} border={`2px solid ${color.GRAY_1}`} flexDirection='column'>
      <Typography fontSize='1.8rem' fontWeight={400} color='primary.main' width='70%' mb={1}>
        {title}
      </Typography>
      <Typography
        fontSize='1.2rem'
        fontWeight={300}
        color='primary.main'
        sx={{
          display: "-webkit-box",
          "-webkit-line-clamp": 7,
          "-webkit-box-orient": "vertical",
          overflow: "hidden",
          "text-overflow": "ellipsis"
        }}
      >
        {desc}
      </Typography>
    </Box>
  );
}

export default ItemCanlendar;
