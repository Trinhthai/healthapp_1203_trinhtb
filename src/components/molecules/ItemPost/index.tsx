import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";

interface ItemBlogEntity {
  img: string;
  date: string;
  title: string;
  tags: string[];
}
function ItemPost({ img, date, title, tags }: ItemBlogEntity) {
  return (
    <Box display='flex' flexDirection='column'>
      <Box height={144} position='relative' sx={{ cursor: "pointer" }}>
        <Box component='img' src={img} width='100%' height='100%' alt='imagepost' sx={{ objectFit: "cover" }} />
        <Box position='absolute' left={0} bottom={0} zIndex={2} width={144} height={24} bgcolor='primary300.main'>
          <Typography fontSize='1.5rem' color='light.main' fontWeight={400} align='center'>
            {date}
          </Typography>
        </Box>
      </Box>
      <Typography
        mt={1}
        fontSize='1.5rem'
        color='primary300.dark'
        fontWeight={300}
        align='center'
        sx={{
          display: "-webkit-box",
          "-webkit-line-clamp": 2,
          "-webkit-box-orient": "vertical",
          overflow: "hidden",
          "text-overflow": "ellipsis"
        }}
      >
        {title}
      </Typography>
      <Box display='flex' gap={1} flexWrap='wrap'>
        {tags.length > 0 &&
          tags.map((itemTag) => (
            <Typography color='primary400.main' fontSize='1.2rem' fontWeight={300}>
              # {itemTag}
            </Typography>
          ))}
      </Box>
    </Box>
  );
}

export default ItemPost;
