import { Line } from "react-chartjs-2";
import "chart.js/auto";

interface ChartLineEntity {
  width?: string | number;
  height?: string | number;
}
function ChartLine({ width, height }: ChartLineEntity) {
  const data = {
    labels: ["6月", "7月", "8月", "9月", "10月", "11月", "12月", "1月", "2月", "3月", "4月", "5月"],
    datasets: [
      {
        data: [70, 68, 60, 58, 52, 45, 35, 30, 29, 20, 21, 10],
        pointRadius: 4,
        order: 1,
        borderColor: "#8FE9D0",
        backgroundColor: "#8FE9D0"
      },
      {
        data: [68, 65, 59, 57, 53, 42, 33, 31, 27, 19, 15, 5],
        pointRadius: 4,
        order: 1,
        borderColor: "#FFCC21",
        backgroundColor: "#FFCC21"
      }
    ]
  };
  const options = {
    scales: {
      x: {
        display: true,
        grid: {
          display: true,
          color: "#777"
        },
        ticks: {
          color: "#fff"
        }
      },
      y: {
        display: false,
        beginAtZero: true,
        grid: {
          display: false
        }
      }
    },
    plugins: {
      legend: {
        display: false
      },
      customCanvasBackgroundColor: {
        color: "lightGreen"
      }
    }
  };

  return <Line data={data} options={options} width={width ?? ""} height={height ?? ""} />;
}

export default ChartLine;
