import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";

interface ItemBannerEntity {
  img: string;
  title: string;
}

function ImageBanner({ img, title }: ItemBannerEntity) {
  return (
    <Box width='100%' height={234} position='relative'>
      <Box component='img' src={img} width='100%' height='100%' alt='imagebanner' sx={{ objectFit: "cover" }} />
      <Box
        position='absolute'
        left={0}
        bottom={0}
        zIndex={2}
        width={120}
        height={32}
        bgcolor='#FFCC21'
        display='flex'
        alignItems='center'
        justifyContent='center'
      >
        <Typography fontSize='1.5rem' fontWeight={400} color='light.main' align='center'>
          {title}
        </Typography>
      </Box>
    </Box>
  );
}

export default ImageBanner;
