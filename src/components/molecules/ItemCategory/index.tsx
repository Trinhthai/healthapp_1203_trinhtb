import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import { Link } from "react-router-dom";

interface ItemCategoryEntity {
  icon: string;
  title: string;
  active?: boolean;
  href?: string;
  count?: number;
}
function ItemCategory({ icon, title, active = false, href, count }: ItemCategoryEntity) {
  return (
    <Link to={href ?? ""}>
      <Box display='flex' alignItems='center' p={1} minWidth={160} sx={{ cursor: "pointer" }}>
        <Box position='relative'>
          <img src={icon} width={32} height={32} alt='logo-app' />
          {count && (
            <Box
              width={16}
              height={16}
              borderRadius={100}
              bgcolor='primary500.main'
              position='absolute'
              right='-5px'
              top='-5px'
              display='flex'
              alignItems='center'
              justifyContent='center'
            >
              <Typography fontSize='1rem' fontWeight={400} alignItems='center' color='light.main'>
                {count}
              </Typography>
            </Box>
          )}
        </Box>
        <Typography ml={1} fontSize='1.6rem' color={active ? "primary400.main" : "light.main"} fontWeight={300}>
          {title}
        </Typography>
      </Box>
    </Link>
  );
}

export default ItemCategory;
