import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";

interface ItemBannerEntity {
  icon: string;
  title: string;
}
function ItemBanner({ icon, title }: ItemBannerEntity) {
  return (
    <Box
      display='flex'
      justifyContent='center'
      alignItems='center'
      flexDirection='column'
      width={134}
      height={116}
      sx={{
        background: "linear-gradient(156deg, #FFCC21 8.26%, #FF963C 91.18%)",
        clipPath: "polygon(25% 0%, 75% 0%, 100% 50%, 75% 100%, 25% 100%, 0% 50%)"
      }}
    >
      <img src={icon} height={56} width={56} alt='imagebanner' />
      <Typography color='light.main' fontSize='2rem' fontWeight={400}>
        {title}
      </Typography>
    </Box>
  );
}

export default ItemBanner;
