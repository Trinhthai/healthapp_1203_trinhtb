import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import color from "constants/color";

interface ItemBlogEntity {
  title: string;
  desc: string;
}
function ItemBlog({ title, desc }: ItemBlogEntity) {
  return (
    <Box
      bgcolor={color.BLACK_02}
      display='flex'
      flexDirection='column'
      justifyContent='center'
      alignItems='center'
      px={1}
      py={3}
    >
      <Typography
        fontSize='2rem'
        fontWeight={400}
        align='center'
        color='primary300.main'
        sx={{ textTransform: "uppercase" }}
      >
        {title}
      </Typography>
      <Box width={56} height={1} bgcolor='light.main' />
      <Typography fontSize='1.8rem' fontWeight={300} color='primary300.main' align='center'>
        {desc}
      </Typography>
    </Box>
  );
}

export default ItemBlog;
