import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import { Link } from "react-router-dom";

import color from "constants/color";

interface ItemSidebarEntity {
  href: string;
  title: string;
  active: boolean;
}
function ItemSidebar({ href, title, active }: ItemSidebarEntity) {
  return (
    <Link to={href}>
      <Box py={2.875} pl={4} bgcolor={color.GRAY} sx={{ cursor: "pointer" }}>
        <Typography
          fontSize='1.8rem'
          fontWeight={300}
          color={active ? "primary300.main" : "light.main"}
          sx={{
            ":hover": {
              color: "primary300.main"
            }
          }}
        >
          {title}
        </Typography>
      </Box>
    </Link>
  );
}

export default ItemSidebar;
