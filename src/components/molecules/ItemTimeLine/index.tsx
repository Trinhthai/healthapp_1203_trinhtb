import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import color from "constants/color";

function ItemTimeLine() {
  return (
    <Box
      width='100%'
      height={40}
      display='flex'
      justifyContent='space-between'
      alignItems='center'
      borderBottom={`2px solid ${color.GRAY}`}
    >
      <Box display='flex' alignItems='center'>
        <Box width={5} height={5} borderRadius={100} bgcolor={color.WHITE} mr={1} />
        <Box display='flex' flexDirection='column'>
          <Typography color='light.main' fontWeight={300} fontSize='1.5rem'>
            家事全般（立位・軽い）
          </Typography>
          <Typography color='primary300.main' fontWeight={400} fontSize='1.5rem'>
            26kcal
          </Typography>
        </Box>
      </Box>
      <Typography color='primary300.main' fontWeight={400} fontSize='1.8rem'>
        10 min
      </Typography>
    </Box>
  );
}

export default ItemTimeLine;
