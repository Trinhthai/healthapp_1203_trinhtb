import { ReactNode } from "react";
import Box from "@mui/material/Box";
import ButtonBase, { ButtonProps } from "@mui/material/Button";

interface ButtonFC {
  variant?: ButtonProps["variant"];
  onClick?: () => void;
  size?: string | number;
  title?: string | number;
  color?: string;
  weight?: string | number;
  radius?: string | number;
  padding?: string | number;
  width?: string | number;
  minWidth?: string | number;
  height?: string | number;
  disabled?: boolean;
  sx?: object;
  background?: string;
  hover?: string;
  type?: ButtonProps["type"];
  InputProps?: ReactNode;
  InputPropsRight?: ReactNode;
}

function Button({
  variant = "contained",
  onClick,
  title,
  size,
  color,
  weight,
  radius = "8px",
  sx,
  padding,
  width,
  minWidth,
  height,
  background = "#000",
  hover = "#000",
  type,
  disabled = false,
  InputProps,
  InputPropsRight
}: ButtonFC) {
  return (
    <Box position='relative'>
      <ButtonBase
        variant={variant}
        onClick={onClick}
        type={type}
        disabled={disabled}
        sx={{
          display: "flex",
          alignItems: "center",
          width: width || "100%",
          padding: padding || "12px 24px",
          fontSize: size || "16px",
          color: color || "#fff",
          textTransform: "none",
          fontWeight: weight || 600,
          borderRadius: radius || "12px",
          position: "static",
          height: height || "",
          boxShadow: "none",
          minWidth: minWidth || width,
          background,
          ":hover": {
            background: hover
          },
          ...sx
        }}
      >
        {InputProps}
        {title}
        {InputPropsRight}
      </ButtonBase>
    </Box>
  );
}

export default Button;
