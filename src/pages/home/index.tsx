import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";

import ChartLine from "components/molecules/ChartLine";
import ImageBanner from "components/molecules/ImageBanner";
import BannerImg from "assets/img/banner.png";
import ArcIcon from "assets/svg/arc.svg";
import MorningIcon from "assets/svg/morning.svg";
import ItemBanner from "components/molecules/ItemBanner";
import lunchIcon from "assets/svg/lunch.svg";
import dinnerIcon from "assets/svg/dinner.svg";
import snackIcon from "assets/svg/snack.svg";
import IconScroll from "assets/svg/component_scroll.svg";
import M01 from "assets/img/m01.jpg";
import L03 from "assets/img/l03.jpg";
import D01 from "assets/img/d01.jpg";
import L01 from "assets/img/l01.jpg";
import L02 from "assets/img/l02.jpg";
import D02 from "assets/img/d02.jpg";
import S01 from "assets/img/s01.jpg";
import Button from "components/atoms/Button";
import COLOR from "constants/color";
import helper from "helpers/scrollToTop";

function HomePage() {
  const LIST_IMG_BANNER = [
    {
      img: M01,
      title: "05.21.Morning"
    },
    {
      img: L03,
      title: "05.21.Lunch"
    },
    {
      img: D01,
      title: "05.21.Dinner"
    },
    {
      img: L01,
      title: "05.21.Dinner"
    },
    {
      img: M01,
      title: "05.20.Morning"
    },
    {
      img: L02,
      title: "05.20.Lunch"
    },
    {
      img: D02,
      title: "05.20.Dinner"
    },
    {
      img: S01,
      title: "05.21.Snack"
    }
  ];

  return (
    <Box width='100%' display='flex' component='section' flexDirection='column'>
      {/* banner */}
      <Box width='100%' display='flex'>
        <Box minWidth={540} position='relative'>
          <img src={BannerImg} height={316} width='100%' alt='imagebanner' />
          <Box position='absolute' top='50%' left='50%' sx={{ transform: "translate(-50%, -50%)" }}>
            <img src={ArcIcon} height={181} width={181} alt='arc' />
            <Box
              position='absolute'
              top='50%'
              left='50%'
              sx={{ transform: "translate(-50%, -50%)" }}
              display='flex'
              alignItems='center'
              gap={0.5}
            >
              <Typography
                fontSize='1.8rem'
                fontWeight={400}
                color='light.main'
                sx={{
                  textShadow: "0px 0px 6px #FCA500"
                }}
              >
                05/21
              </Typography>
              <Typography
                fontSize='2.5rem'
                fontWeight={400}
                color='light.main'
                sx={{
                  textShadow: "0px 0px 6px #FCA500"
                }}
              >
                75%
              </Typography>
            </Box>
          </Box>
        </Box>
        <Box
          display='flex'
          flex={1}
          height={316}
          width='100%'
          bgcolor={COLOR.BLACK_01}
          justifyContent='center'
          alignItems='center'
          py={2}
        >
          <ChartLine />
        </Box>
      </Box>
      {/* lunch */}
      <Box width='100%' display='flex' flexDirection='column' alignItems='center'>
        <Box width='100%' maxWidth={894} my={3.1}>
          <Box display='flex' alignItems='center' justifyContent='space-evenly'>
            <ItemBanner icon={MorningIcon} title='Morning' />
            <ItemBanner icon={lunchIcon} title='Lunch' />
            <ItemBanner icon={dinnerIcon} title='Dinner' />
            <ItemBanner icon={snackIcon} title='Snack' />
          </Box>
        </Box>
      </Box>
      {/* image banner */}
      <Box
        display='grid'
        gridTemplateColumns='repeat(4, 1fr)'
        gap={1}
        maxWidth={960}
        margin='0 auto'
        mb={3.5}
        position='relative'
      >
        {LIST_IMG_BANNER.map((itemBanner) => (
          <ImageBanner key={Math.random()} img={itemBanner.img} title={itemBanner.title} />
        ))}

        <Box position='absolute' right='-7%' top='50%' sx={{ cursor: "pointer" }} onClick={helper.scrollToTop}>
          <Box component='img' src={IconScroll} alt='scrollicon' />
        </Box>
      </Box>
      <Box mb={8}>
        <Button
          background={COLOR.LINEAR_PRIMARY}
          hover={COLOR.LINEAR_PRIMARY}
          title='記録をもっと見る'
          width={296}
          size='1.8rem'
          weight={300}
          sx={{
            margin: "0 auto"
          }}
        />
      </Box>
    </Box>
  );
}

export default HomePage;
