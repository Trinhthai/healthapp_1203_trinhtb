import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";

import ItemRecord from "components/molecules/ItemRecord";
import MyRecommend1 from "assets/img/MyRecommend-1.jpg";
import MyRecommend2 from "assets/img/MyRecommend-2.jpg";
import MyRecommend3 from "assets/img/MyRecommend-3.jpg";
import color from "constants/color";
import ChartLine from "components/molecules/ChartLine";
import Button from "components/atoms/Button";
import ItemTimeLine from "components/molecules/ItemTimeLine";
import ItemCanlendar from "components/molecules/ItemCanlendar";
import IconScroll from "assets/svg/component_scroll.svg";
import helper from "helpers/scrollToTop";

function RecordPage() {
  const LIST_CALENDAR = [
    {
      title: "2021.05.21   23:25",
      desc: `私の日記の記録が一部表示されます。
テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…`
    },
    {
      title: "2021.05.21   23:25",
      desc: `私の日記の記録が一部表示されます。
テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…`
    },
    {
      title: "2021.05.21   23:25",
      desc: `私の日記の記録が一部表示されます。
テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…`
    },
    {
      title: "2021.05.21   23:25",
      desc: `私の日記の記録が一部表示されます。
テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…`
    },
    {
      title: "2021.05.21   23:25",
      desc: `私の日記の記録が一部表示されます。
テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…`
    },
    {
      title: "2021.05.21   23:25",
      desc: `私の日記の記録が一部表示されます。
テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…`
    },
    {
      title: "2021.05.21   23:25",
      desc: `私の日記の記録が一部表示されます。
テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…`
    },
    {
      title: "2021.05.21   23:25",
      desc: `私の日記の記録が一部表示されます。
テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…`
    }
  ];

  return (
    <Box display='flex' flexDirection='column' maxWidth={960} margin='0 auto' component='section'>
      <Box display='flex' justifyContent='space-between' my={7}>
        <ItemRecord img={MyRecommend1} title='BODY RECORD' subTitle='自分のカラダの記録' />
        <ItemRecord img={MyRecommend2} title='BODY RECORD' subTitle='自分のカラダの記録' />
        <ItemRecord img={MyRecommend3} title='BODY RECORD' subTitle='自分のカラダの記録' />
      </Box>

      {/* chartjs */}
      <Box
        display='flex'
        width='100%'
        bgcolor={color.BLACK}
        px={3}
        py={2}
        flexDirection='column'
        mb={7}
        position='relative'
      >
        <Box display='flex' alignSelf='flex-start'>
          <Typography
            color='light.main'
            fontSize='1.5rem'
            fontWeight={400}
            sx={{ textTransform: "uppercase" }}
            width='45%'
          >
            BODY RECORD
          </Typography>
          <Typography color='light.main' fontSize='2.2rem' fontWeight={400} sx={{ textTransform: "uppercase" }}>
            2021.05.21
          </Typography>
        </Box>
        <Box width='100%' height={304} display='flex' justifyContent='center' alignItems='center'>
          <ChartLine width={870} height={304} />
        </Box>
        <Box pl={1} display='flex' alignSelf='flex-start' gap={2} ml={1}>
          <Button
            title='日'
            color={color.PRIMARY}
            background={color.WHITE}
            hover={color.WHITE}
            radius='11px'
            width={56}
            height={24}
            size='1.5rem'
            weight={300}
          />
          <Button
            title='週'
            color={color.PRIMARY}
            background={color.WHITE}
            hover={color.WHITE}
            radius='11px'
            width={56}
            height={24}
            size='1.5rem'
            weight={300}
          />
          <Button
            title='月'
            color={color.PRIMARY}
            background={color.WHITE}
            hover={color.WHITE}
            radius='11px'
            width={56}
            height={24}
            size='1.5rem'
            weight={300}
          />
          <Button
            title='年'
            color={color.WHITE}
            background={color.PRIMARY}
            hover={color.PRIMARY}
            radius='11px'
            width={56}
            height={24}
            size='1.5rem'
            weight={300}
          />
        </Box>

        <Box position='absolute' right='-7%' bottom='0%' sx={{ cursor: "pointer" }} onClick={helper.scrollToTop}>
          <Box component='img' src={IconScroll} alt='scrollicon' />
        </Box>
      </Box>

      {/* time line */}
      <Box display='flex' width='100%' bgcolor={color.BLACK} px={3} py={2} flexDirection='column' mb={7}>
        <Box display='flex' alignSelf='flex-start'>
          <Typography
            color='light.main'
            fontSize='1.5rem'
            fontWeight={400}
            sx={{ textTransform: "uppercase" }}
            width='45%'
          >
            MY EXERCISE
          </Typography>
          <Typography color='light.main' fontSize='2.2rem' fontWeight={400} sx={{ textTransform: "uppercase" }}>
            2021.05.21
          </Typography>
        </Box>
        <Box
          overflow='auto'
          pr={2}
          sx={{
            "::-webkit-scrollbar-track": {
              background: color.GRAY,
              borderRadius: "6px"
            },
            "::-webkit-scrollbar-thumb": {
              background: color.PRIMARY
            }
          }}
        >
          <Box display='grid' gridTemplateColumns='repeat(2, 1fr)' columnGap={5} rowGap={1} height={264}>
            {Array.from({ length: 100 }, () => (
              <ItemTimeLine key={Math.random() * 10} />
            ))}
          </Box>
        </Box>
      </Box>

      {/* calendar */}
      <Box display='flex' width='100%' flexDirection='column' mb={2.875}>
        <Typography
          color='primary.main'
          fontSize='2.2rem'
          fontWeight={400}
          sx={{ textTransform: "uppercase", lineHeight: "27px" }}
        >
          MY DIARY
        </Typography>
        <Box display='grid' gridTemplateColumns='repeat(4, 1fr)' rowGap={1.5} columnGap={1.5}>
          {LIST_CALENDAR.map((item) => (
            <ItemCanlendar key={Math.random() * 2} title={item.title} desc={item.desc} />
          ))}
        </Box>
      </Box>

      <Box mb={6}>
        <Button
          background={color.LINEAR_PRIMARY}
          hover={color.LINEAR_PRIMARY}
          title='自分の日記をもっと見る'
          width={296}
          size='1.8rem'
          weight={300}
          sx={{
            margin: "0 auto"
          }}
        />
      </Box>
    </Box>
  );
}

export default RecordPage;
