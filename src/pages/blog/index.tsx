import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";

import ItemBlog from "components/molecules/ItemBlog";
import ItemPost from "components/molecules/ItemPost";
import Column1 from "assets/img/column-1.jpg";
import Column2 from "assets/img/column-2.jpg";
import Column3 from "assets/img/column-3.jpg";
import Column4 from "assets/img/column-4.jpg";
import Column5 from "assets/img/column-5.jpg";
import Column6 from "assets/img/column-6.jpg";
import Column7 from "assets/img/column-7.jpg";
import Column8 from "assets/img/column-8.jpg";
import Button from "components/atoms/Button";
import color from "constants/color";
import helper from "helpers/scrollToTop";
import IconScroll from "assets/svg/component_scroll.svg";

function BlogPage() {
  const LIST_BLOG = [
    {
      title: "RECOMMENDED COLUMN",
      desc: "オススメ"
    },
    {
      title: "RECOMMENDED DIET",
      desc: "ダイエット"
    },
    {
      title: "RECOMMENDED BEAUTY",
      desc: "美容"
    },
    {
      title: "RECOMMENDED HEALTH",
      desc: "健康"
    }
  ];

  const LIST_POST = [
    {
      img: Column1,
      title: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
      date: "2021.05.17   23:25",
      tags: ["#魚料理", "#和食", "#DHA"]
    },
    {
      img: Column2,
      title: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
      date: "2021.05.17   23:25",
      tags: ["#魚料理", "#和食", "#DHA"]
    },
    {
      img: Column3,
      title: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
      date: "2021.05.17   23:25",
      tags: ["#魚料理", "#和食", "#DHA"]
    },
    {
      img: Column4,
      title: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
      date: "2021.05.17   23:25",
      tags: ["#魚料理", "#和食", "#DHA"]
    },
    {
      img: Column5,
      title: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
      date: "2021.05.17   23:25",
      tags: ["#魚料理", "#和食", "#DHA"]
    },
    {
      img: Column6,
      title: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
      date: "2021.05.17   23:25",
      tags: ["#魚料理", "#和食", "#DHA"]
    },
    {
      img: Column7,
      title: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
      date: "2021.05.17   23:25",
      tags: ["#魚料理", "#和食", "#DHA"]
    },
    {
      img: Column8,
      title: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
      date: "2021.05.17   23:25",
      tags: ["#魚料理", "#和食", "#DHA"]
    }
  ];

  return (
    <>
      <Box display='flex' flexDirection='column' maxWidth={960} margin='0 auto' component='section' position='relative'>
        <Box display='grid' gridTemplateColumns='repeat(4, 1fr)' columnGap={4} my={7}>
          {LIST_BLOG.map((item) => (
            <ItemBlog key={Math.random() * 3} title={item.title} desc={item.desc} />
          ))}
        </Box>

        <Box display='grid' gridTemplateColumns='repeat(4, 1fr)' columnGap={1} rowGap={2}>
          {LIST_POST.map((itemPost) => (
            <ItemPost
              key={Math.random() * 2}
              img={itemPost.img}
              title={itemPost.title}
              date={itemPost.date}
              tags={itemPost.tags}
            />
          ))}
        </Box>
        <Box mb={6} mt={3.25}>
          <Button
            background={color.LINEAR_PRIMARY}
            hover={color.LINEAR_PRIMARY}
            title='コラムをもっと見る'
            width={296}
            size='1.8rem'
            weight={300}
            sx={{
              margin: "0 auto"
            }}
          />
        </Box>
      </Box>

      <Box
        display='flex'
        flexDirection='column'
        position='absolute'
        right='15px'
        bottom='110px'
        width={139}
        height={91}
        p={1}
        sx={{
          background:
            "linear-gradient(0deg, rgba(0, 0, 0, 0.20) 0%, rgba(0, 0, 0, 0.20) 100%), rgba(217, 217, 217, 0.10)",
          cursor: "pointer"
        }}
        onClick={helper.scrollToTop}
      >
        <Box component='img' width={48} height={48} src={IconScroll} alt='scrollicon' />
        <Typography fontSize='1.6rem' fontWeight={500} ml={1}>
          go to top
        </Typography>
      </Box>
    </>
  );
}

export default BlogPage;
