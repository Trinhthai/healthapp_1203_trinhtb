import { makeStyles, createStyles } from "@material-ui/core";
import { createTheme } from "@mui/material";

export const theme = createTheme({
  typography: {
    fontFamily: ["Inter", "sans-serif", "Noto Sans JP", "sans-serif"].join(",")
  },
  palette: {
    primary: {
      main: "#414141"
    },
    light: { main: "#fff" },
    primary300: {
      main: "#FFCC21",
      dark: "#414141"
    },
    primary400: {
      main: "#FF963C",
      dark: "#FCA500"
    },
    primary500: {
      main: "#EA6C00"
    },
    secondary300: {
      main: "#8FE9D0"
    }
  }
});

const useStyles: any = makeStyles(() =>
  createStyles({
    "@global": {
      ":root": {
        "--black": "#000000"
      },

      html: {
        boxSizing: "border-box",
        fontSize: "62.5%",
        msTextSizeAdjust: "100%",
        webkitTextSizeAdjust: "100%",
        scrollBehavior: "smooth"
      },
      "*": {
        boxSizing: "inherit"
      },
      body: {
        margin: 0,
        padding: 0,
        color: "var(--black)",
        fontWeight: 400,
        fontSize: "1.6rem",
        overflowY: "overlay",
        overflowX: "unset"
      },
      "input:hover, textarea:hover": {
        cursor: `text`
      },
      "input::-webkit-outer-spin-button, input::-webkit-inner-spin-button": {
        webkitAppearance: "none",
        margin: 0
      },
      "button:hover, a:hover": {
        cursor: `pointer`
      },
      button: {
        outline: "none",
        border: "none",
        padding: 0,
        margin: 0,
        background: "none",
        overflow: "visible"
      },
      "article, aside, details, figcaption, figure, footer, header, hgroup, main, menu, nav, section, summary": {
        display: "block"
      },
      "audio, canvas, progress, video": {
        display: "inline-block",
        verticalAlign: "baseline"
      },
      "audio:not([controls])": {
        display: "none",
        height: 0
      },
      "[hidden], template": {
        display: "none"
      },
      a: {
        backgroundColor: "transparent",
        textDecoration: "none"
      },
      "a:active, a:hover": {
        outline: 0
      },
      "abbr[title]": {
        borderBottom: "1px dotted"
      },
      "b, strong": {
        fontWeight: "bold"
      },
      dfn: {
        fontStyle: "italic"
      },
      h1: {
        fontSize: "2em",
        margin: "0.67em 0"
      },
      mark: {
        background: "#ff0",
        color: "#000"
      },
      small: {
        fontSize: "80%"
      },
      "sub, sup": {
        fontSize: "75%",
        lineHeight: 0,
        position: "relative",
        verticalAlign: "baseline"
      },
      sup: {
        top: "-0.5em"
      },
      sub: {
        bottom: "-0.25em"
      },
      img: {
        border: 0
      },
      "svg:not(:root)": {
        overflow: "hidden"
      },
      figure: {
        margin: "1em 40px"
      },
      hr: {
        MozBoxSizing: "content-box",
        boxSizing: "content-box",
        height: 0
      },
      pre: {
        overflow: "auto"
      },
      "code, kbd, pre, samp": {
        fontFamily: "monospace, monospace",
        fontSize: "1em"
      },
      "button, input, optgroup, select, textarea": {
        color: "inherit",
        font: "inherit",
        margin: 0
      },
      "button, select": {
        textTransform: "none"
      },
      'button, html input[type="button"], input[type="reset"], input[type="submit"]': {
        webkitAppearance: "button",
        cursor: "pointer"
      },
      "button[disabled], html input[disabled]": {
        cursor: "default"
      },
      "button::-moz-focus-inner, input::-moz-focus-inner": {
        border: 0,
        padding: 0
      },
      input: {
        lineHeight: "normal"
      },
      'input[type="checkbox"], input[type="radio"]': {
        boxSizing: "border-box",
        padding: 0
      },
      'input[type="number"]::-webkit-inner-spin-button, input[type="number"]::-webkit-outer-spin-button': {
        height: "auto"
      },
      'input[type="search"]': {
        webkitAppearance: "textfield",
        MozBoxSizing: "content-box",
        webkitBoxSizing: "content-box",
        boxSizing: "content-box"
      },
      'input[type="search"]::-webkit-search-cancel-button, input[type="search"]::-webkit-search-decoration': {
        webkitAppearance: "none"
      },
      fieldset: {
        // border: `1px solid ${COLOR.PRIMARY} !important`,
        margin: "0 2px",
        padding: "0.35em 0.625em 0.75em"
      },
      legend: {
        border: 0,
        padding: 0
      },
      textarea: {
        overflow: "auto"
      },
      optgroup: {
        fontWeight: "bold"
      },
      table: {
        borderCollapse: "collapse",
        borderSpacing: 0
      },
      "td, th": {
        padding: 0
      },
      "html *::-webkit-scrollbar": {
        borderRadius: 0,
        width: "6px"
      },
      "html *::-webkit-scrollbar-thumb": {
        borderRadius: "3px",
        backgroundColor: "rgba(22, 24, 35, .2)"
      },
      "html *::-webkit-scrollbar-track": {
        borderRadius: 0,
        backgroundColor: "rgba(0, 0, 0, 0)"
      },
      "video::-webkit-media-controls-panel": {
        display: "none"
      },
      "video::-webkit-media-controls": {
        display: "none"
      },
      "video::-webkit-media-controls-start-playback-button": {
        display: "none"
      },
      "video::-webkit-media-controls-volume-slider": {
        display: "none"
      },
      "video::-webkit-media-controls-mute-button": {
        display: "none"
      },
      "video::-webkit-media-controls-timeline": {
        display: "none"
      },
      "video::-webkit-media-controls-current-time-display": {
        display: "none"
      }
    }
  })
);
function GlobalStyle() {
  useStyles();
  return null;
}
export default GlobalStyle;
