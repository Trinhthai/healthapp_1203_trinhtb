import { Route, Routes } from "react-router-dom";
import MainLayout from "layouts/MainLayout";
import HomePage from "pages/home";
import RecordPage from "pages/record";
import BlogPage from "pages/blog";

function RootRouter() {
  return (
    <Routes>
      <Route path='/' Component={MainLayout}>
        <Route path='/' Component={HomePage} />
        <Route path='/record' Component={RecordPage} />
        <Route path='/blog' Component={BlogPage} />
      </Route>
    </Routes>
  );
}

export default RootRouter;
