import { PaletteColor, SimplePaletteColorOptions } from "@mui/material/styles";

declare module "@mui/material/styles" {
  interface Palette {
    light: PaletteColor;
    primary300: PaletteColor;
    primary400: PaletteColor;
    primary500: PaletteColor;
    secondary300: PaletteColor;
  }

  interface PaletteOptions {
    light: SimplePaletteColorOptions;
    primary300: SimplePaletteColorOptions;
    primary400: SimplePaletteColorOptions;
    primary500: SimplePaletteColorOptions;
    secondary300: SimplePaletteColorOptions;
  }
}
