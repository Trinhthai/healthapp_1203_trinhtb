import { Outlet } from "react-router-dom";
import Box from "@mui/material/Box";
import MainHeader from "./Header";
import MainFooter from "./Footer";

function MainLayout() {
  return (
    <Box>
      <MainHeader />
      <Box minHeight='100vh' component='section'>
        <Outlet />
      </Box>
      <MainFooter />
    </Box>
  );
}

export default MainLayout;
