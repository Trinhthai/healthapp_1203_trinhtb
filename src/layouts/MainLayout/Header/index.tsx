import Box from "@mui/material/Box";
import { Link, useLocation } from "react-router-dom";
import { useEffect, useState } from "react";

import ItemCategory from "components/molecules/ItemCategory";
import Logo from "assets/img/logo.png";
import IconNote from "assets/svg/icon_note.svg";
import IconMedal from "assets/svg/icon_medal.svg";
import IconInfo from "assets/svg/icon_info.svg";
import IconMenu from "assets/svg/icon_menu.svg";
import IconClose from "assets/svg/icon_close.svg";
import ItemSidebar from "components/molecules/ItemSidebar";
import color from "constants/color";

function MainHeader() {
  const { pathname } = useLocation();

  const [statusSidebar, setStatusSidebar] = useState<boolean>(false);

  const HEADERS = [
    {
      icon: IconNote,
      title: "自分の記録",
      active: pathname === "/record",
      href: "/record"
    },
    {
      icon: IconMedal,
      title: "チャレンジ",
      active: pathname === "/blog",
      href: "/blog"
    },
    {
      icon: IconInfo,
      title: "自分の記録",
      active: pathname === "/info",
      href: "/",
      count: 1
    }
  ];

  const SIDEBARS = [
    {
      title: "自分の記録",
      active: pathname === "/record",
      href: "/record"
    },
    {
      icon: IconMedal,
      title: "体重グラフ",
      active: false,
      href: "/"
    },
    {
      title: "目標",
      active: false,
      href: "/"
    },
    {
      title: "選択中のコース",
      active: false,
      href: "/"
    },
    {
      title: "コラム一覧",
      active: pathname === "/blog",
      href: "/blog"
    },
    {
      title: "設定",
      active: false,
      href: "/"
    }
  ];

  const activeSidebar = () => setStatusSidebar(!statusSidebar);

  useEffect(() => {
    setStatusSidebar(false);
  }, [pathname]);
  return (
    <>
      <Box
        display='flex'
        alignItems='center'
        bgcolor='primary.main'
        width='100%'
        height={64}
        justifyContent='center'
        component='header'
        position='sticky'
        top={0}
        left={0}
        right={0}
        zIndex={99}
      >
        <Box width='100%' maxWidth={960} display='flex' justifyContent='space-between' alignItems='center'>
          <Link to='/'>
            <img src={Logo} width={109} height={40} alt='logo-app' />
          </Link>
          <Box display='flex' alignItems='center'>
            <Box display='flex' alignItems='center'>
              {HEADERS.map((itemHead) => (
                <ItemCategory
                  icon={itemHead.icon}
                  title={itemHead.title}
                  href={itemHead.href}
                  active={itemHead.active}
                  count={itemHead.count}
                />
              ))}
            </Box>
            <Box onClick={activeSidebar} sx={{ cursor: "pointer" }}>
              <img src={statusSidebar ? IconClose : IconMenu} width={32} height={32} alt='icon-menu' />
            </Box>
          </Box>
        </Box>
      </Box>

      {/* sidbar */}
      <Box
        width={280}
        height='100vh'
        display='flex'
        flexDirection='column'
        position='fixed'
        top={64}
        right={statusSidebar ? 0 : "-1000px"}
        bottom={0}
        bgcolor={color.GRAY}
        zIndex={10}
        sx={{
          transition: "all .5s"
        }}
      >
        {SIDEBARS.map((itemSide) => (
          <ItemSidebar key={Math.random()} title={itemSide.title} href={itemSide.href} active={itemSide.active} />
        ))}
      </Box>
    </>
  );
}

export default MainHeader;
