import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import { Link } from "react-router-dom";

function MainFooter() {
  const FooterItems = [
    {
      title: "会員登録",
      link: "/"
    },
    {
      title: "運営会社",
      link: "/"
    },
    {
      title: "利用規約",
      link: "/"
    },
    {
      title: "個人情報の取扱について",
      link: "/"
    },
    {
      title: "特定商取引法に基づく表記",
      link: "/"
    },
    {
      title: "お問い合わせ",
      link: "/"
    }
  ];
  return (
    <Box
      display='flex'
      alignItems='center'
      bgcolor='primary.main'
      width='100%'
      height={128}
      justifyContent='center'
      component='footer'
    >
      <Box width='100%' maxWidth={894} display='flex' justifyContent='space-between' alignItems='center'>
        <Box display='flex' gap={5.625}>
          {FooterItems.map((itemFoot) => (
            <Link to={itemFoot.link} key={Math.random()}>
              <Typography fontSize='1.1rem' color='light.main'>
                {itemFoot.title}
              </Typography>
            </Link>
          ))}
        </Box>
      </Box>
    </Box>
  );
}

export default MainFooter;
