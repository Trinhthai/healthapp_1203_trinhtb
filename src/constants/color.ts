export default {
  LINEAR_PRIMARY: "linear-gradient(33deg, #FFCC21 8.75%, #FF963C 86.64%)",
  PRIMARY: "#FFCC21",
  BLACK: "#414141",
  BLACK_01: "#2E2E2E",
  BLACK_02: "#2E2E2E",
  WHITE: "#fff",
  GRAY: "#777777",
  GRAY_1: "#707070"
} as const;
