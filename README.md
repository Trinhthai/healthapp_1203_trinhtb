# HealthApp_1203_trinhtb

# name_chanel_slack: `ext_test_thai-ba-trinh_frontend_v_20230925`

| Operation/ENV | Version |
| ------------- | ------- |
| NPM           | 8.11.0  |
| NodeJS        | 16.16.0 |
| ReactJS       | 18.2.0  |

# Link Figma: https://www.figma.com/file/7qqT3dvv5OagaRlUFK01vB/HealthApp_1203

#Lib use in project

| Name Lib         | Version |
| ---------------- | ------- |
| react-router-dom | 6.16.0  |
| chart.js         | 4.4.0   |
| react-chartjs-2  | 5.2.0   |
| @mui/material    | 5.14.10 |

# Run project mode dev:

Step1:

```
npm install
```

step 2:

```
npm run start
```
